
jQuery(document).ready(function () {
    const header = document.getElementById("header");
    const btns = header.getElementsByClassName("header-btn");
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function () {

            /** Header Buttons are activated when scrolled into view */

            // var current = document.getElementsByClassName("active");
            // current[0].className = current[0].className.replace(" active", "");
            // this.className += " active";

            // behavior: "smooth" is not supported on safari
            if (this.name === "intro") {
                $("html, body").animate({ scrollTop: "0" }, 1000);
            } else {
                //document.getElementById(this.name).scrollIntoView({ behavior: "smooth" });
                console.log($('#' + this.name).first().position().top)
                //window.scrollTo(0, $('#'+this.name).first().position().top - 60).animate()
                $("html, body").animate(
                    { scrollTop: $('#' + this.name).first().position().top - 60 }, 1000);
            }
        });
    }
})

function focusButton() {
    const aboutY = $("#about").first().position().top;
    const introY = $("#intro").first().position().top;
    const workY = $("#work").first().position().top;
    const contactY = $("#contact").first().position().top;
    const skillsY = $('.skills').first().position().top;
    // change button when scrolling through page
    if (window.matchMedia('(max-width: 768px)').matches) {
        if ((workY + contactY) / 2 <= window.scrollY + 200) {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            //document.getElementById("contact-btn").className += " active";
            $("#contact-btn").addClass('active', 1000);

        } else if ((workY + skillsY) / 2 <= window.scrollY) {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            $("#work-btn").addClass('active', 1000);

        } else if (aboutY / 2 <= window.scrollY) {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            //document.getElementById("about-btn").className += " active";
            $("#about-btn").addClass('active', 1000);
            //Fill up skill bar when intro is visible

            if (jQuery('.skills').first().position().top / 2 <= window.scrollY) {
                jQuery('.skillbar, .adress').each(function () {

                    jQuery(this).find('.skillbar-bar').animate({
                        width: jQuery(this).attr('data-percent')
                    }, 3000);

                });
            }

        } else if (introY / 2 <= window.scrollY) {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            document.getElementById("intro-btn").className += " active";

        }
    } else {
        if ((workY + contactY) / 2 <= window.scrollY + 100) {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            //document.getElementById("contact-btn").className += " active";
            $("#contact-btn").addClass('active', 1000);

        } else if ((workY + aboutY) / 2 <= window.scrollY) {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            $("#work-btn").addClass('active', 1000);

        } else if (aboutY / 2 <= window.scrollY) {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            //document.getElementById("about-btn").className += " active";
            $("#about-btn").addClass('active', 1000);
            //Fill up skill bar when intro is visible

            jQuery('.skillbar, .adress').each(function () {

                jQuery(this).find('.skillbar-bar').animate({
                    width: jQuery(this).attr('data-percent')
                }, 3000);

            });

            // jQuery('.adress').each(function () {
            //     jQuery(this).find('.address-item').animate({
            //         width: '100%'
            //     }, 3000);
            // });

        } else if (introY / 2 <= window.scrollY) {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            document.getElementById("intro-btn").className += " active";

        }
    }

    if (window.scrollY >= 70) {
        var header = document.getElementById("header-wrapper");
        header.style.backgroundColor = "#555"
    } else if (window.scrollY <= 70) {
        document.getElementById("header-wrapper").style.background = "none";
    }

}